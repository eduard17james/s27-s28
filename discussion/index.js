const protocol = require('http');

// designate and identify a virtual point where the network connection will start and end.

const port = 4000; 

// create connection

protocol.createServer((req, res) => {
	res.write(`Welcome to the Server`);
	res.end();
}).listen(port)

console.log(`Server is running on Port ${port}...`);